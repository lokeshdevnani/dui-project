import containers from './containers/container';
import ext from './ext';

try {
  ext();

  if (typeof window !== "undefined") {
    window.__WIDTH = window.__DEVICE_DETAILS().screen_width;
    window.__ID = 1;
    window.__NODE_ID = 1;
    window.__SCREEN_INDEX = -1;
    window.__CACHED_SCREENS = [];
    window.__PROXY_FN = {};
    window.__FN_INDEX = 0;
    window.__ROOTSCREEN = null;
    window.callUICallback = function() {
      var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
      var fName = args[0]
      var functionArgs = args.slice(1)

      window.__PROXY_FN[fName].call(null, ...functionArgs);
    }
    window.onBackpressed = () => {
      if (window.handleBackPress) { window.handleBackPress() }
    }
  }
  containers.init()
} catch (e) {
  console.error(e)
  Android.throwError(e.stack);
}
