module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			//console.log("showScreen " ,action "Payload ",payload );
			dispatcher("SCREEN", action, payload)
		},
		sayHello : (payload) => {
			dispatcher("HELLO_WORLD_SCREEN", "STORE_DATA", {message : "Hello World!"});
		},
		proceed : (message) => {
			dispatcher("HELLO_WORLD_SCREEN", "STORE_RESPONSE", {message : message});
		}
	};
}