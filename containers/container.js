//Logger
//const logger = require("../utils/logger");

//Reducer
const ScreenReducer = require("../state_machines/Screens");
const HelloWorldScreenReducer = require("../state_machines/HelloWorldState");
const SecondScreenReducer = require("../state_machines/SecondScreenState");

const reducer = require("@juspay/mystique-backend").stateManagers.reducer({
	"SCREEN" : ScreenReducer,
	"HELLO_WORLD_SCREEN" : HelloWorldScreenReducer,
	"SECOND_SCREEN" : SecondScreenReducer
});

const uiHandler = require("@juspay/mystique-backend").uiHandlers.android;
var dispatcher;

// Screens
const RootScreen = require("../views/RootScreen");
const HelloWorldScreen = require("../views/HelloWorldScreen");
const SecondScreen = require("../views/SecondScreen");

// ScreenActions
const RootScreenActions = require("../actions/RootScreenActions");
const HelloWorldActions = require("../actions/HelloWorldActions");
const SecondScreenActions = require("../actions/SecondScreenActions");


var determineScreen = (state, dispatcher) => {
	var screen;
	switch (state.global.currScreen) {
		case "HELLO_WORLD_SCREEN" : 
		 		screen = new (HelloWorldScreen(dispatcher, HelloWorldActions))(null, null, state);
		 		break;
	 	case "SECOND_SCREEN" : 
	 		console.log("SECOND_SCREEN ");
	 		screen = new (SecondScreen(dispatcher, SecondScreenActions))(null, null, state);
	 		break; 
	 	default : 
	 		throw new Error("Current screen not handled "+ state.global.currScreen);
	 		break;
	}

	return screen;
}

var res;
var currView;
var CURRENT_SCREEN = null;

var Containers = {
	handleStateChange : (data) => {
		var state = data.state.global;
		console.log("STATE : " , data.state);
		if(state.currScreen) {
			console.log("SCREEN : ",state.currScreen);
			currView = (new (RootScreen(dispatcher, RootScreenActions))({}, determineScreen(data.state, dispatcher), data));
			return { render : currView.render()};
		} else {
			return currView.handleStateChange(data) || {};
		}
	}
}

dispatcher = require("@juspay/mystique-backend").stateManagers.dispatcher(Containers, uiHandler, reducer);

module.exports = {
	init : () => {
		dispatcher("SCREEN", "INIT_UI", {});
	}
}