var dom = require("@juspay/mystique-backend").doms.android;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;


class EditText extends View {
	constructor(props, children) {
		super(props, children);

		this.setIds([
			'id'
		]);
	}

	render() {
		var params = this.props;

		return (
			<editText 
				id={this.props.id?this.props.id:this.idSet.id}
				width = {this.props.width}
				height = {this.props.height}
				hint = {this.props.hint?this.props.hint:""}
				text = {this.props.text}
				backgroundTint = {this.props.backgroundTint}/>
		)
	}
}

module.exports = EditText;