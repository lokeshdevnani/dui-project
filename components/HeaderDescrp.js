var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;



import tinyColor from '../utils/tinyColor';
import Fab from '../components/Fab';



import HorizontalCardView from '../components/HorizontalCardView';




class HeaderDescrp extends View {
	constructor(props, children) {
		super(props, children);

		this.setIds([
			'id'
		]);
	}
render() {


		this.layout = (
			
				
				<LinearLayout
					width="130"
					height="wrap_content"
					background = "#ffffff"
					orientation="vertical"
					gravity="center">

					
                    <TextView 
											hint={this.props.header}
											gravity = "center"
											
											fontSize="0,6"
											margin="0,0,0,0"
											/>
                    <TextView 
											hint={this.props.descrp}
											gravity = "center"
											
											fontSize="0,3"
											margin="0,0,0,50"
											/>
							
			
                    	
                </LinearLayout>
				
			
		)

		return this.layout.render();
	}
}

module.exports = HeaderDescrp;