const dom = require("@juspay/mystique-backend").doms.android;
const View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;

import styles from '../styles/Toolbar_css'
import coreStyles from '../styles/Core_css'

class Toolbar extends View {

  constructor(props, children) {
      super(props, children);
      this.displayName = "toolbar";
      this.setIds([
      	'contentRow2Wrapper',
      	'customerInfo',
      	'customerId',
      	'contentRow3Wrapper',
      	'accountNo',
      	'contentRow4Wrapper',
      	'mobileNo'
      ]);
  }

  showAccountNo(text) {
  	var _this = this;

  	var cmd1 = this.set({
  		id: _this.idSet.contentRow3Wrapper,
  		visibility: "visible",
  	});

  	var cmd2 = this.set({
  		id: _this.idSet.accountNo,
  		text: text + '',
  	});

  	return cmd1 + cmd2;
  }

  showOnlyAppBar() {
  	var _this = this;

  	var cmd1 = this.set({
  		id: _this.idSet.contentRow2Wrapper,
  		visibility: "gone",
  	});

  	var cmd2 = this.set({
  		id: _this.idSet.contentRow3Wrapper,
  		visibility: "gone",
  	});

  	var cmd3 = this.set({
  		id: _this.idSet.contentRow4Wrapper,
  		visibility: "gone",
  	});

  	return cmd1 + cmd2 + cmd3;
  }

  showMobileNo(text) {
  	var _this = this;

  	var cmd1 = this.set({
  		id: _this.idSet.contentRow4Wrapper,
  		visibility: "visible",
  	});

  	var cmd2 = this.set({
  		id: _this.idSet.mobileNo,
  		text: text + '',
  	});

  	return cmd1 + cmd2;
  }

  hideCustomerId() {
  	var _this = this;

  	var cmd = this.set({
  		id: _this.idSet.customerInfo,
  		visibility: "gone",
  	});

  	return cmd;
  }

  showCustomerId(text) {
  	var _this = this;

  	var cmd1 = this.set({
  		id: _this.idSet.customerInfo,
  		visibility: "visible",
  	});

  	var cmd2 = this.set({
  		id: _this.idSet.customerId,
  		text: text + '',
  	});

  	return cmd1 + cmd2;
  }

  getFourthRow() {
  	if (!this.props.mobileNo)
  	return <linearLayout/>

  	return (<linearLayout id={this.idSet.contentRow4Wrapper} style = {styles.contentRow4Wrapper} width="match_parent" orientation="vertical" >
	  	<linearLayout style={styles.separator}/>
			<linearLayout style = {styles.contentRow4}>
				<textView fontSize="0,4" text="RMNOTP" color="#ffffff" />
				<textView id={this.idSet.mobileNo} fontSize="0,4" style = {styles.contentRow4Right} text={this.props.mobileNo} />
			</linearLayout>	
		</linearLayout>)
  }

  getThirdRow() {
  	if (!this.props.accountNo)
  	return <linearLayout/>

  	return (
  		<linearLayout id={this.idSet.contentRow3Wrapper} style = {styles.contentRow3Wrapper} width="match_parent" orientation="vertical">
  		<linearLayout style={styles.separator}/>
	  		<linearLayout style = {styles.contentRow3}>
				<textView fontSize="0,4" text="From Savings Account" color="#ffffff" />
				<textView id={this.idSet.accountNo} fontSize="0,4" style = {styles.contentRow3Right} text={this.props.accountNo}/>
		</linearLayout>	
		</linearLayout>	)
  }

  getCustomerRow() {
  	if (!this.props.customerId)
  	return <linearLayout/>

  	return (<linearLayout id={this.idSet.customerInfo} style={styles.customerInfo}>
			<linearLayout width="45"/>
			<linearLayout width="match_parent" padding="0,5,0,5">
				<textView id={this.idSet.customerId} fontSize="0,4"  text={"Customer ID " + this.props.customerId} color={coreStyles.COLOR_1}/>
				<textView fontSize="0,4" text="Tap to Edit" style = {styles.edit} />
			</linearLayout>
		</linearLayout>)
  }

  getSecondRow() {
  	if (!this.props.location)
  	return <linearLayout/>

  	return (
  	<linearLayout id={this.idSet.contentRow2Wrapper} style = {styles.contentRow2Wrapper} width="match_parent" orientation="vertical">
  		<linearLayout style={styles.separator}/>
			<linearLayout style = {styles.contentRow2}>
				<linearLayout height="50" width="30">
					<imageView imageUrl="makemytrip"/>
				</linearLayout>

				<linearLayout style = {styles.contentRow2Left}>
					<textView typeface='bold'  text={this.props.location} color="#ffffff" fontSize="0,3.5"/>
					<textView  text={this.props.time} color="#ffffff" fontSize="0,3.5"/>
				</linearLayout>
				<textView style = {styles.contentRow2Right} text={"₹ " + this.props.price} />
			</linearLayout>
		</linearLayout>)
  }

  render() {
  	var secondRow = this.getSecondRow();
  	var thirdRow = this.getThirdRow();
  	var fourthRow = this.getFourthRow();
  	var customerRow = this.getCustomerRow();

    return (
    	<linearLayout style = {styles.toolbar}>
	    	<linearLayout style = {styles.toolbarInfoWrapper}>
	    		<linearLayout style = {styles.backButtonWrapper}>
	    			<imageView onClick={this.props._onBackClick} width="match_parent" imageUrl="arrow_left"/>
	    		</linearLayout>	

	    		<linearLayout style = {styles.contentWrapper}>
	    			<linearLayout style = {styles.contentRow1}>
		    			<linearLayout width="80">
		    				<imageView imageUrl="axisbanklogo"/>
		    			</linearLayout>
		    			<textView text="Internet Banking" style={styles.netBankingText}/>
		    		</linearLayout>

		    		{secondRow}	
		    		{thirdRow}
		    		{fourthRow}

	    		</linearLayout>	
    		</linearLayout>
    		{customerRow}
    	</linearLayout>
    )
  }
}

module.exports = Toolbar;