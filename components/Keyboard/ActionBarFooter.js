const dom = require("@juspay/mystique-backend").doms.android;
const View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
const bankConfig = require("../../config/banks.js");
const DEFAULTS = {
  BACKGROUND:"#FFFFFF",
  WIDTH:"match_parent",
  HEIGHT:"90",
  TEXT:"Remember Customer ID",
  CORNERRADIUS:"100",
  ALIGNPARENTBOTTOM:"true,-1",
  ALIGNPARENTRIGHT:"true,-1"

}

class ActionBarFooter extends View {

  constructor(props, children) {
      super(props, children);
      this.setIds(["floatingButton","nextIcon"]);
      this.setDefaults();
  }

  setDefaults = () => {
    this.shouldSaveCustomerId = true;
    this.width = this.props.width || DEFAULTS.WIDTH;
    this.height = this.props.height || DEFAULTS.HEIGHT;
    this.background = this.props.background || DEFAULTS.BACKGROUND;
    this.nextIconBackground = this.nextIconBackground || "#FF105092"
    this.text = this.props.text || DEFAULTS.TEXT;
    this.saveCustomerIdText = this.props.saveCustomerIdText || "Remember CustomerID";
    this.saveCustomerIdTextSize = this.props.saveCustomerIdTextSize || "6";
    this.saveCustomerIdTextColor = this.props.saveCustomerIdTextColor || "#000000";
    this.cornerRadius = this.props.cornerRadius || DEFAULTS.CORNERRADIUS;
    this.alignParentBottom = this.props.alignParentBottom || DEFAULTS.ALIGNPARENTBOTTOM;
    this.alignParentRight = this.props.alignParentRight || DEFAULTS.ALIGNPARENTRIGHT;
  }

  onSubmit = () => {
    if(typeof(this.props.onSubmit)=="function"){
      this.props.onSubmit(this.value);
    }
  }

  onSaveCustomerIdClicked = () => {
    this.shouldSaveCustomerId = !this.shouldSaveCustomerId;
    if(typeof(this.props.shouldSaveCustomerId)=="function") {
      this.props.shouldSaveCustomer(this.shouldSaveCustomerId);
    }
  };

  render() {
    var layout = (
      <relativeLayout background={this.background}  width={this.width} height={this.height}>
            <relativeLayout width={this.width}
              gravity="center_vertical"
              alignParentBottom={this.alignParentBottom}
              height="60" background="#FFE2E5E6">
                <checkBox
                  width="wrap_content"
                  checked="true"
                  height="wrap_content"
                  margin="20,0,0,0"
                  buttonTint={this.saveCustomerIdTextColor}
                  size={this.saveCustomerIdTextSize}
                  text={this.saveCustomerIdText}
                  onClick={this.onSaveCustomerIdClicked}/>
            </relativeLayout>
             <linearLayout id={this.idSet.floatingButton} 
                width="75" height="75" margin="0,0,10,0"
                gravity="center" alignParentRight={this.alignParentRight}
                cornerRadius={this.cornerRadius}
                background={this.nextIconBackground}>
                <imageView
                  id={this.idSet.nextIcon}
                  onClick={this.onSubmit}
                  gravity="center"
                  width="40"
                  height="40"
                  imageUrl="check"/>
             </linearLayout>   
      </relativeLayout>
    );
    return layout;
  }
}

module.exports = ActionBarFooter;