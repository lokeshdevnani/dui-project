var dom = require("@juspay/mystique-backend").doms.android;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var Connector = require("@juspay/mystique-backend").connector;

class KeyboardKey extends View {
	
	constructor(props, children) {
		super(props, children);
		this.key = props.key;
		this.gravity = props.gravity || "center";
		this.setIds(["id"]);
	}

	onKeyDown() {
		Android.runInUI(this.set({
			id:this.idSet.id,
			background:this.props.feedbackBg
		}),null);
		setTimeout((function(){
			Android.runInUI(this.set({
				id:this.idSet.id,
				background:this.props.background
			}),null);
		}).bind(this),parseInt(this.props.feedbackDuration));
		this.props.onKeyDown(this.idSet.id,this.key);
	};

	render() {
		if(this.props.isImage) {
			return (<relativeLayout id={this.idSet.id} background={this.props.background}
	         weight={this.props.weight}  height="wrap_content" onClick={this.onKeyDown.bind(this)} >
	          <imageView id={this.idSet.id+"111"} width="match_parent" gravity={this.gravity}
					padding={this.props.padding}
					imageUrl={this.props.backgroundDrawable}/>
	        </relativeLayout>);
		} else {
			return (<relativeLayout id={this.idSet.id} background={this.props.background}
	         weight={this.props.weight} height="wrap_content"  onClick={this.onKeyDown.bind(this)}>
	          <textView id={this.idSet.id+"111"} width="match_parent" gravity={this.gravity}
					padding={this.props.padding} color={this.props.color}
					textSize={this.props.textSize} text={this.key} />
	        </relativeLayout>);
		}
	}
}

module.exports = KeyboardKey;