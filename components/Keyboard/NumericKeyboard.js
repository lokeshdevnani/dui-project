const dom = require("@juspay/mystique-backend").doms.android;
const View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
const Connector = require("@juspay/mystique-backend").connector;
const KeyboardKey = require("./KeyboardKey.js");
const Keyboard = require("./Keyboard.js");

const numberLayout = [
	['1','2','3'],
	['4','5','6'],
	['7','8','9'],
    ['','0','Del']
];

const DEFAULTS = {
	KEYBOARD_BACKGROUND:"#ffffff",
	KEY_BACKGROUND:"#ffffff",
	KEYBOARD_WIDTH:"match_parent",
	KEYBOARD_HEIGHT:"wrap_content",
	KEY_SIZE:"14",
	KEY_COLOR:"#121212",
	BACKSPACE_DRAWABLE:"backspace",
	FEEDBACK_DURATION:"50",
	FEEDBACK_BACKGROUND:"#cccccc",
	KEY_PADDING:"5,5,5,5",
	VALUE:"",
	KEY_PADDING:"10,10,10,10",
	IMAGE_PADDING:"10,10,10,10",
	KEY_ROW_MARGIN:"30,0,30,0"
};

class NumericKeyboard extends Keyboard {

	constructor(props, children) {
		super(props, children);
		this.setDefaults();
	}

	setDefaults() {
		this.background = this.props.keyboardBackground || DEFAULTS.KEYBOARD_BACKGROUND;
		this.width = this.props.width || DEFAULTS.KEYBOARD_WIDTH;
		this.height = this.props.height || DEFAULTS.KEYBOARD_HEIGHT;
		this.keyPadding = this.props.keyPadding || DEFAULTS.KEY_PADDING;
		this.keySize = this.props.keySize || DEFAULTS.KEY_SIZE
		this.keyColor = this.props.keyColor || DEFAULTS.KEY_COLOR;
		this.keyBackground = this.props.keyBackground || DEFAULTS.KEY_BACKGROUND;
		this.backspaceDrawable = this.props.backspaceDrawable || DEFAULTS.BACKSPACE_DRAWABLE;
		this.feedbackDuration = this.props.feedbackDuration || DEFAULTS.FEEDBACK_DURATION;
		this.feedbackBg = this.props.feedbackBackground || DEFAULTS.FEEDBACK_BACKGROUND;
		this.value = this.props.value || DEFAULTS.VALUE;
		this.keyRowMargin = this.props.keyRowMargin || DEFAULTS.KEY_ROW_MARGIN;
	}

	onKeyDown(id,key) {
		switch(key.toLowerCase()) {
			case 'del':return this.onDelete();
			default: {
				this.changeValue(key);
				return;
			}
		}
	};

	onValueChange() {
		if(typeof(this.props.onValueChange)=="function"){
			this.props.onValueChange(this.value)
		}
	}

	getKeyProps(key) {
		let props = {key:key,padding:DEFAULTS.KEY_PADDING};
		if(key.toLowerCase()=="del") {
			props.isImage = true;
			props.padding = DEFAULTS.IMAGE_PADDING;
			props.backgroundDrawable = this.backspaceDrawable;
		} else if(key.toLowerCase()=="") {
			props.feedbackBg = this.background;
			props.padding = DEFAULTS.KEY_PADDING
		}
		return props;
	};

	renderKeyboard() {
		let keyboardRows = [];
		numberLayout.map((row,index)=>{
			let keys = [];
			row.map((key,index)=>{
				let keyboardKey = this.getKeyboardKey(this.getKeyProps(key));
				keys.push(keyboardKey);
			});
			keyboardRows.push(this.getKeyboardRow({},keys));
		});
		return keyboardRows;
	}

	render() {
		let layout =
		<linearLayout orientation="vertical" width={this.width} height={this.height}>
			{this.renderKeyboard()}
		</linearLayout>
		return layout;
	}
}

module.exports = NumericKeyboard;