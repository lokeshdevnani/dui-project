const dom = require("@juspay/mystique-backend").doms.android;
const View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
const LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
const Button = require("@juspay/mystique-backend").androidViews.Button;
const Connector = require("@juspay/mystique-backend").connector;
const KeyboardKey = require("./KeyboardKey.js");
const Keyboard = require("./Keyboard.js");
const KeyboardActionBar = require("./KeyboardActionBar");
const MIN_SWIPE_DISTANCE = 150;
const LEFT_TO_RIGHT = "L2R";
const RIGHT_TO_LEFT = "R2L";
const DEFAULTS = {
	KEYBOARD_BACKGROUND:"#ffffff",
	KEY_BACKGROUND:"#ffffff",
	KEYBOARD_WIDTH:"match_parent",
	KEYBOARD_HEIGHT:"wrap_content",
	KEY_SIZE:"8",
	KEY_COLOR:"#121212",
	CAPS_DRAWABLE:"caps",
	SPACE_DRAWABLE:"spacebar",
	BACKSPACE_DRAWABLE:"backspace",
	FEEDBACK_DURATION:"50",
	FEEDBACK_BACKGROUND:"#cccccc",
	SHOW_SYMBOL_ROW:false,
	SHOW_NUMERIC_ROW:false,
	CASE:"both",
	DEFAULT_CASE:"lower",
	KEY_PADDING:"10,10,10,10",
	IMAGE_PADDING:"10,10,10,10",
	IS_PASSWORD:false,
	KEY_ROW_MARGIN:"0,0,0,0",
	VALUE:"",
	HINT:"Start Typing"
};

const symbol1 = [
	['!','@','#','$','%','^','&','*','(',')'],
	['"','\'','-',':','\\','[',']','{','}','<','>'],
	['CapsPlaceHolder','=','|','?','/','_',';',',','.','Del'],
];

const symbols = [
	['!','@','#','$','%','^','&','*','(',')'],
	['~','"','\'','-','s','s','s',']','{','}'],
	['space','<','>','|','?','/','\\;','s','s','.','Del']
];

const characters = [
	'q','w','e','r','t','y','u','i','o','p',
	'a','s','d','f','g','h','j','k','l',
	'z','x','c','v','b','n','m'
];
const actionKeys = [
	'Caps','Del','Space','?123'
];
const numbers = [
	'1','2','3','4','5','6','7','8','9','0'
];
const qwertyLayout = [
	['q','w','e','r','t','y','u','i','o','p'],
    ['a','s','d','f','g','h','j','k','l'],
    ['CapsPlaceHolder','z','x','c','v','b','n','m','Del']
];
const passwordLayout = [
	'toggleChar','showPass','next'
];

const isLetter = function(str) {
  return str && str.length === 1 && str.match(/[a-z,A-Z]/i);
};

const isDigit = function(str) {
	return str && !isNaN(str)
};

class AlphaNumericKeyboard extends Keyboard {
	
	constructor(props, children) {
		super(props, children);
		this.setDefaults();
		this.setIds(["toggleChars","togglePassword","submit","symbolsKeyboard","alphaNumericKeyboard"]);
	};

	setDefaults() {
		this.background = this.props.keyboardBackground || DEFAULTS.KEYBOARD_BACKGROUND;
		this.width = this.props.width || DEFAULTS.KEYBOARD_WIDTH;
		this.height = this.props.height || DEFAULTS.KEYBOARD_HEIGHT;
		this.keyPadding = this.props.keyPadding || DEFAULTS.KEY_PADDING;
		this.keySize = this.props.keySize || DEFAULTS.KEY_SIZE
		this.keyColor = this.props.keyColor || DEFAULTS.KEY_COLOR;
		this.keyBackground = this.props.keyBackground || DEFAULTS.KEY_BACKGROUND;
		this.keyRowMargin = this.props.keyRowMargin || DEFAULTS.KEY_ROW_MARGIN;
		this.backspaceDrawable = this.props.backspaceDrawable || DEFAULTS.BACKSPACE_DRAWABLE;
		this.spacebarDrawable = this.props.spacebarDrawable || DEFAULTS.SPACE_DRAWABLE;
		this.capsDrawable = this.props.capsDrawable || DEFAULTS.CAPS_DRAWABLE;
		this.feedbackDuration = this.props.feedbackDuration || DEFAULTS.FEEDBACK_DURATION;
		this.feedbackBg = this.props.feedbackBackground || DEFAULTS.FEEDBACK_BACKGROUND;
		this.showSymbolRow = this.getPropValue("showSymbolRow",DEFAULTS.SHOW_SYMBOL_ROW);
		this.showNumericRow = this.getPropValue("showNumericRow",DEFAULTS.SHOW_NUMERIC_ROW);
		this.isPassword = this.getPropValue("isPassword",DEFAULTS.IS_PASSWORD);
		this.defaultCase = this.props.defaultCase || DEFAULTS.DEFAULT_CASE;
		this.hint = this.props.hint || DEFAULTS.HINT; 
		this.value = this.props.value || DEFAULTS.VALUE;
		this.isShowingPassword = false;
		this.hasSelectedShowPassword = false;
		this.currentCase = this.defaultCase;
		this.currentKeys = [];
		this.symbolKeys = [];
		this.toggleCharValue = "?123";
		this.symbolIndex=-1;
		this.callToActionRow = null;
	};

	getSymbolsRow() {
		let keys = [];
		symbols[0].map((key,index)=>{
			let keyboardKey = this.getKeyboardKey(this.getKeyProps(key));
			keys.push(keyboardKey);
			this.symbolKeys.push(keyboardKey);
		});
		let keyboardRow = this.getKeyboardRow({},keys);
		return keyboardRow;
	};

	getNumbersRow() {
		let keys = [];
		numbers.map((key,index)=>{
			let keyboardKey = this.getKeyboardKey(this.getKeyProps(key));
			keys.push(keyboardKey);
			this.currentKeys.push(keyboardKey);
		});
		let keyboardRow = this.getKeyboardRow({},keys);
		return keyboardRow;
	};

	
	getQwertyLayout(characterCase) {
		let keyboardRows = [];
		qwertyLayout.map((row,index)=>{
			let keys = [];
			row.map((key,index)=>{
				if(characterCase=="lower") {
					key = key.toLowerCase();
				} else {
					key = key.toUpperCase();
				}
				let keyboardKey = this.getKeyboardKey(this.getKeyProps(key));
				keys.push(keyboardKey);
				this.currentKeys.push(keyboardKey);
			});
			keyboardRows.push(this.getKeyboardRow({},keys));
		});
		return keyboardRows;
	};

	getSymbolsLayout() {
		let keyboardRows = [];
		symbols.map((row,index)=>{
			let keys = [];
			row.map((key,index)=>{
				let props = this.getKeyProps(key);
				let keyboardKey = this.getKeyboardKey(props);
				keys.push(keyboardKey);
				this.currentKeys.push(keyboardKey);
			});
			keyboardRows.push(this.getKeyboardRow({},keys));
		});
		return keyboardRows;
	};

	getKeyProps(key) {
		let props = {key:key,padding:DEFAULTS.KEY_PADDING};
		if(key.toLowerCase()=="a") {
			props.weight="0.96";
			props.padding = "10,10,13,10";
			props.gravity="right";
		} else if(key.toLowerCase()=="l") {
			props.weight="0.96";
			props.padding = "13,10,10,10";
			props.gravity="left";
		} else if(key.toLowerCase()=="capsplaceholder") {
			props.weight="0.98";
			props.isImage = true;
			props.backgroundDrawable = this.capsDrawable;
			props.padding = DEFAULTS.IMAGE_PADDING;
		} else if(key.toLowerCase()=="del") {
			props.weight="0.98";
			props.isImage = true;
			props.padding = DEFAULTS.IMAGE_PADDING;
			props.backgroundDrawable = this.backspaceDrawable;
		} else if(key.toLowerCase()=="space") {
			props.weight="0.98";
			props.isImage = true;
			props.padding = DEFAULTS.IMAGE_PADDING;
			props.backgroundDrawable = this.spacebarDrawable;
		}
		return props;
	};

	onValueChange() {
		if(this.isPassword) {
			if(this.hasSelectedShowPassword) {
				if(this.callToActionRow) {
					if(this.isShowingPassword) {
						this.callToActionRow.changeShowPasswordText(this.value);
					} else {
						this.callToActionRow.changeShowPasswordText(this.getMaskedValue());
					}
				}
			}
		} else {
			this.callToActionRow.changeShowPasswordText(this.value);
		}
		if(typeof(this.props.onValueChange)=="function"){
			this.props.onValueChange(this.value)
		}
	}

	changeCase(toCase) {
		this.currentKeys.map(function(keyboardKey){
			if(!isLetter(keyboardKey.key)) {
				return;
			}
			if(toCase=="upper") {
				keyboardKey.key = keyboardKey.key.toUpperCase();
			} else {
				keyboardKey.key = keyboardKey.key.toLowerCase();
			}
			Android.runInUI(keyboardKey.set({
				id:keyboardKey.idSet.id+"111",
				text:keyboardKey.key
			}),null);
		});
	};

	toggleCase() {
		if(this.currentCase == "lower") {
			this.changeCase("upper");
			this.currentCase="upper";
		} else if(this.currentCase == "upper") {
			this.changeCase("lower");
			this.currentCase="lower";
		}
	}

	getMaskedValue() {
		return Array(this.value.length+1).join("x");
	}

	onTogglePassword() {
		this.hasSelectedShowPassword = true;
		if(this.isShowingPassword) {
			if(this.value.length>0) {
				this.callToActionRow.changeShowPasswordText(this.getMaskedValue());
			} else {
				this.callToActionRow.changeShowPasswordText("");
			}
			this.isShowingPassword = false;
		} else {
			this.callToActionRow.changeShowPasswordText(this.value);
			this.isShowingPassword = true;
		}
	}

	onSubmit() {
		if(typeof(this.props.onSubmit)=="function"){
			this.props.onSubmit(this.value);
		}
	}

	changeToggleCharText() {
		this.callToActionRow.changeSwitchActionText(this.toggleCharValue);
	}

	toggleKeyboardVisibility(toKeyboard) {
		let symbolsKeyboardVisibility = "gone";
		let alphaNumericKeyboardVisibility = "visible";
		if(toKeyboard=="symbol") {
			symbolsKeyboardVisibility="visible";
			alphaNumericKeyboardVisibility="gone";
		}
		Android.runInUI(this.set({
			id:this.idSet.alphaNumericKeyboard,
			visibility:alphaNumericKeyboardVisibility
		}),null);
		Android.runInUI(this.set({
			id:this.idSet.symbolsKeyboard,
			visibility:symbolsKeyboardVisibility
		}),null);
	}

	onToggleChars() {
		if(this.toggleCharValue=="?123") {
			this.toggleCharValue="ABC";
			this.changeToggleCharText();
			this.toggleKeyboardVisibility("symbol");
		} else {
			this.toggleCharValue="?123";
			this.changeToggleCharText();
			this.toggleKeyboardVisibility("alphaNumeric");
		}
	}

	onKeyDown(id,key) {
		switch(key.toLowerCase()) {
			case 'capsplaceholder':return this.toggleCase();
			case 'del':return this.onDelete();
			default: {
				this.changeValue(key);
				return;
			}
		}
	};

	renderCallToActionRow() {
		let props = {
			background:this.background,
			showPassword:this.isPassword,
			onSwitchAction:this.onToggleChars.bind(this),
			showPasswordTextColor:this.keyColor,
			onNextAction:this.onSubmit.bind(this)
		};
		if(this.isPassword) {
			props.onShowPassword=this.onTogglePassword.bind(this);
		}
		this.callToActionRow = new KeyboardActionBar(props);
		return this.callToActionRow.render();
	};

	renderKeyboard(type) {
		let rows = [];
		if(type=="alphaNumeric") {
			if(this.showSymbolRow) {
				rows.push(this.getSymbolsRow());
			}
			if(this.showNumericRow) {
				rows.push(this.getNumbersRow());
			}
			rows = rows.concat(this.getQwertyLayout(this.defaultCase));
		} else {
			rows.push(this.getNumbersRow());
			rows = rows.concat(this.getSymbolsLayout());
		}
		return rows;
	};

	render() {
		let layout =
		<linearLayout orientation="vertical" width={this.width} height={this.height}>
			<frameLayout width="match_parent"
			        height="wrap_content">
				<linearLayout
			        id={this.idSet.alphaNumericKeyboard}
			        orientation="vertical"
			        width="match_parent"
			        height="wrap_content"
			        background={this.background}
			        >
					{this.renderKeyboard("alphaNumeric")}
				</linearLayout>
				<linearLayout
					id={this.idSet.symbolsKeyboard}
			        orientation="vertical"
			        width="match_parent"
			        height="wrap_content"
			        visibility="gone"
			        background={this.background}
			        >
			    {this.renderKeyboard("symbols")}
				</linearLayout>
			</frameLayout>
			{this.renderCallToActionRow()}
		</linearLayout>
		return layout;
	}
}

module.exports = AlphaNumericKeyboard;