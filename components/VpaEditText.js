var dom = require("@juspay/mystique-backend").doms.android;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;

class VpaEditText extends View {
	constructor(props, children) {
		super(props, children);

		this.setIds([
			'editText',
			'hint'
		]);

		this.textChange ="";
		this.focused=false;

		this.EnterFlag = "true";

	}

	showError() {
		var _this = this;

		var cmd = this.set({
			id: _this.idSet.empty,
			visibility: 'visible',
			a_scaleY: '1'
		});

		Android.runInUI(cmd, null);
	}

	handleFocus = (isFocused) => {
		var _this = this;
		var cmd;
		if(!this.focused){
			this.focused=true;

			Android.runInUI(this.set({
					id:this.idSet.editText,
					text:this.props.initText
			}), null);
		}
	}

	handleChange = (value) => {

		if(this.props.entryType!="Text"){


			if(!this.focused)
				return;

		if(this.EnterFlag=="false"){
			value[0]=value[0].replace(this.props.appendText,"");
		}
		var backFlag = false;
		if(this.EnterFlag=="true" && value[0].length < this.textChange.length){
			value[0]=this.props.initText;
			backFlag=true;
		}
		
		value[0]=value[0].replace(this.props.initText,"");		

		if(value[0]==""){
			value[0]=this.props.initText;
			this.EnterFlag="true";
		}
		else
			this.EnterFlag="false";
			if(this.textChange!=value[0] || backFlag)
			{
				
				backFlag=false;

				var hyphenFlag = true;
				if(this.props.entryType=="Expiry" && this.EnterFlag=="false" && this.textChange.length>value[0].length)
				{
					hyphenFlag=false;
				}

				this.textChange=value[0];
				
				if(this.props.entryType=="Card"){

					if(this.EnterFlag=="false" && value[0].length>6)
					{
						var cmd="android.widget.Toast->makeText:ctx_ctx," + "cs_" + this.props.errorText + ",i_10;show;";
						Android.runInUI(cmd,null);
						value[0]=value[0].substring(0,6);
					}

					if(this.EnterFlag=="false")
					{
						value[0]=this.props.appendText.concat(value[0]);
						
					}
				

					
				}

				else if(this.props.entryType=="Expiry"){

					if(this.EnterFlag=="false" && value[0].length>="2" && hyphenFlag ){

						if(value[0].length==2)
							value[0]=value[0].concat("-");
						else if(value[0][2]!='-'){
							
							value[0]=value[0].substring(0,2)+"-"+value[0].substring(2,4);
						}	
					}
					if(!hyphenFlag && value[0].length=="3"){
						value[0]=value[0].substring(0,2);
					}				
					if(this.EnterFlag=="false"&&value[0].length>"5"){

						var cmd="android.widget.Toast->makeText:ctx_ctx," + "cs_" + this.props.errorText + ",i_10;show;";
						Android.runInUI(cmd,null);
						value[0]=value[0].substring(0,5);

					}
				}

				this.props.changeText(value[0]);
				var cmd=this.set({

						id:this.idSet.editText,
						text:value[0]
					}) + "get_view->setSelection:i_"+value[0].length+";";

				Android.runInUI(cmd, null);
			}
		} else {
			this.props.changeText(value[0]);
		}
	}



	render() {
		return (
				<linearLayout padding="0,0,0,0" background="#FFFFFF"  margin={this.props.margin}  width="match_parent" orientation="horizontal">
					<editText id={this.idSet.editText} 
					gravity={this.props.gravity ? this.props.gravity : "center"} 
					hint={this.props.hint}
					text = {this.props.text} 
					padding="0,0,0,0"
					id = {this.props.id}
					color = {this.props.color}
					margin = {this.props.margin?this.props.margin : "0,0,0,0"}
					onChange={this.handleChange}
					onFocus={this.handleFocus}
					fontSize="0,3"
					background="#ffffff" 
					width="230"
					height="40"
					inputType="1" 
					maxLength="19"/>
				</linearLayout>	
		)
	}
}

module.exports = VpaEditText;