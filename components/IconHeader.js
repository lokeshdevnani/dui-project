var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;



import tinyColor from '../utils/tinyColor';
import Fab from '../components/Fab';
import Toolbar from '../components/Toolbar';
import HorizontalCardView from '../components/HorizontalCardView';


class IconHeader extends View {
	constructor(props, children) {
		super(props, children);
		this.setIds([
			'id'
		]);
	}
	
render() {


		this.layout = (

				<LinearLayout
					width="match_parent"
					height="wrap_content"
					background = "#ffffff"
					orientation="vertical"
					gravity="center">
					<ImageView
						imageUrl="axis_pay_logo"
						height="wrap_content"
						width="wrap_content"
						margin="20,50,20,40"/>
                    <TextView 
						hint="Pay through ANY Bank Account" 
						gravity = "center"
						fontSize="0,4"
						margin="0,0,0,50"
						/>	
                </LinearLayout>
		)

		return this.layout.render();
	}
}

module.exports = IconHeader;


