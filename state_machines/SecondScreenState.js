const objectAssign = require('object-assign');

var localState = {
}

module.exports = function(action, payload, state) {
	var globalState = {
    currScreen : null
  }
	switch (action) {
		case "ANIMATE_SCREEN":
			localState.nextAction = "ANIMATE_SCREEN";
			break;
		/*case "STORE_DATA":
			localState.message = payload.message
			localState.nextAction = "SAY_WELCOME";
			break;*/
		case "STORE_RESPONSE":
			localState.message = payload.message;
			localState.nextAction = "SHOW_NEXT_SCREEN";
			break;	
		default :
			throw new Error("Invalid action Passed :  action name" + action);

	}
	return objectAssign({}, state, {global : objectAssign({}, state.global, globalState), local: localState});
}
