const objectAssign = require('object-assign');

var localState = {
	screens : []
}

module.exports = function(action, payload, state) {

	switch (action) {
		case "INIT_UI":
			localState.isInit =  true;
			localState.currScreen = "HELLO_WORLD_SCREEN";
			break;
		case "HELLO_WORLD_SCREEN" : 
			localState.currScreen = "HELLO_WORLD_SCREEN";
			break;
		case "SHOW_SECOND_SCREEN":
			localState.currScreen = "SECOND_SCREEN";
			break;
		case "SHOW_PREVIOUS_SCREEN":
			var index = localState.screens.length - 1;
			if(index > 0) {
				localState.screens.pop();
				localState.currScreen = localState.screens[localState.screens.length - 1];
			} else {
				localState.currScreen = null;
				localState.nextAction = "SHOW_QUIT_TRANSACTION_POPUP"
			}
			break;
		default :
			throw new Error("Invalid action Passed :  action name" + action);

	}
	if(action !== "SHOW_PREVIOUS_SCREEN") {
		localState.screens.push(localState.currScreen);
	}
	return objectAssign({}, state, {global : objectAssign({}, state.global, localState), local: {}});
}
