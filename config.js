const API = "http://upi-wrapper-beta.ap-south-1.elasticbeanstalk.com";

console.log("We are on version - Config 0.0.2");

//try catch

var data = UPI.decryptAndloadFile("index_bundle.jsa");
if(data && data.length && data.length > 0) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.innerHTML = data;
    var head = document.getElementsByTagName( "head" )[ 0 ];
    head.appendChild( script );
} else {
    console.log("STARTING NATIVE FLOW");
    UPI.startNativeFlow();
}

window.onFileDownload = (status, URL) => {
    console.log(status, URL);
}

UPI.downloadFile(API+"/index_bundle.jsa", "onFileDownload");
