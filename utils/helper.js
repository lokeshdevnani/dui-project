const SALT_DELIMITER = "|";
const Olive = require("../services/Olive.js");
const R = require("ramda");

const credConfigs = {
  "MPIN":{
    CredAllowed:[{
      type:"PIN",
      subtype:"MPIN",
      dType:null,
      dLength:null
    }]
  },
  "SET_MPIN": {
    CredAllowed:[{
      type:"PIN",
      subtype:"MPIN",
      dType:null,
      dLength:null
    },{
      type:"OTP",
      subtype:"SMS",
      dType:"NUM",
      dLength:6
    }]
  },
  "CHANGE_MPIN": {
    CredAllowed:[{
      type:"PIN",
      subtype:"MPIN",
      dType:null,
      dLength:null
    },{
      type:"PIN",
      subtype:"NMPIN",
      dType:null,
      dLength:null
    }]
  }
};

const getCredAllowedString = (type,mpinLength, mpinType) => {
  let config = credConfigs[type];
  for(let i=0;i<config.CredAllowed.length;i++) {
    let credConfig = config.CredAllowed[i];
    if(credConfig.type=="PIN") {
      credConfig.dLength = mpinLength;
      credConfig.dType = mpinType;
    }
  }
  return JSON.stringify(config);
};

const getRandomNumber = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

const getNpciConfiguration = (bank) => {
  let configuration = {
    payerBankName: bank,
    color: "#CD1C5F",
    backgroundColor: "#FFFFFF"
  }
  return JSON.stringify(configuration);
};

const getNpciTrustStr = (txnId) => {
  let trustStr = txnId + SALT_DELIMITER +
    JBridge.getPackageName() + SALT_DELIMITER +
    JBridge.getKey("userName", null) + SALT_DELIMITER +
    JBridge.getDeviceId()
  return NPCICL.trustCred(trustStr, JBridge.getKey("npciToken", null));
};

const getPayTrustStr = (txnId, txnAmount, payeeVpa, payerVpa) => {
  let trustStr = parseFloat(txnAmount).toFixed(2)+SALT_DELIMITER + txnId + SALT_DELIMITER +
    payerVpa + SALT_DELIMITER + payeeVpa + SALT_DELIMITER +
    JBridge.getPackageName() + SALT_DELIMITER +
    JBridge.getKey("userName", null) + SALT_DELIMITER +
    JBridge.getDeviceId()
  return NPCICL.trustCred(trustStr, JBridge.getKey("npciToken", null));
};

const getPayInfoArray = () => {
  return JSON.stringify([{ "name": "mobileNumber", "value": JBridge.getKey("userName", null) }]);
};

const getInfoArray = (payeeName, payerVpa, txnAmount, remarks, txnId) => {
  return JSON.stringify([{ name: "payeeName", "value": payeeName },
    { "name": "txnAmount", "value": txnAmount && parseFloat(txnAmount).toFixed(2) },
    { "name": "note", "value": remarks },
    { "name": "refId", "value": txnId },
    { "name": "refUrl", "value": "HTTPS:\/\/AXISBANK.CO.IN\/" }
  ])
}

const getNpciSalt = (txnId, txnAmount, payeeVpa, payerVpa) => {
  return JSON.stringify({
    txnId: txnId,
    txnAmount: txnAmount && parseFloat(txnAmount).toFixed(2),
    appId: JBridge.getPackageName(),
    deviceId: JBridge.getDeviceId(),
    mobileNumber: JBridge.getKey("userName", null),
    payeeAddr: payeeVpa,
    payerAddr: payerVpa
  });
};


exports.getAccountPayload = (type,txnId, account, npciKeys) => {
  let payload = {};
  payload.credAllowedString = getCredAllowedString(type,account.mpinLength,account.mpinType);
  payload.listKeys = NPCICL.decodeNPCIXmlKeys(npciKeys);
  payload.configuration = getNpciConfiguration(account.bankName);
  payload.trustStr = getNpciTrustStr(txnId);
  payload.payInfoArray = getPayInfoArray();
  payload.keyCode = "NPCI";
  payload.salt = getNpciSalt(txnId);
  payload.languagePref = "en_Us";
  return payload;
};

exports.getNpciPayPayload = (type, txnId, account, npciKeys, orderDetails, beneDetails, vpa) => {
  let payload = {};
  payload.credAllowedString = getCredAllowedString(type, account.mpinLength, account.mpinType);
  payload.listKeys = NPCICL.decodeNPCIXmlKeys(npciKeys);
  payload.keyCode = "NPCI";
  payload.salt = getNpciSalt(txnId, orderDetails.amount,beneDetails.beneVpa,vpa);
  payload.languagePref = "en_Us";
  payload.trustStr = getPayTrustStr(txnId, orderDetails.amount, beneDetails.beneVpa, vpa);
  payload.configuration = getNpciConfiguration(account.bankName, account);
  payload.payInfoArray = getInfoArray(beneDetails.beneName, vpa, orderDetails.amount, orderDetails.remarks, txnId);
  return payload;
};

exports.listKeyResponseCheck = (listKeyResponse) => {
  if (!listKeyResponse || listKeyResponse.error) {
    return false;
  }
  if (!listKeyResponse.npciKeys || typeof listKeyResponse.npciKeys != "object") {
    return false;
  }
  if (listKeyResponse.npciKeys.result != "Success" || listKeyResponse.npciKeys.data == null) {
    return false;
  }
  return true;
}

exports.npciTokenResponseCheck = (npciTokenResponse) => {
  if (!npciTokenResponse || npciTokenResponse.error) {
    return false;
  }
  if (!npciTokenResponse.npciToken || typeof npciTokenResponse.npciToken != "object") {
    return false;
  }
  if (npciTokenResponse.npciToken.token == null) {
    return false;
  }
  return true;
}

exports.getCachedIndex = (screenName) => {
  return R.findIndex(screen=>screen.key==screenName)(window.__CACHED_SCREENS);
}

exports.getUniqueId = () => {
  return getRandomNumber() + getRandomNumber() + getRandomNumber() + getRandomNumber() +
    getRandomNumber() + getRandomNumber() + getRandomNumber() + getRandomNumber();
};
