var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;

const CURRENT_SCREEN = "HELLO_WORLD_SCREEN";

class HelloWorldScreen extends View {

  constructor(props, children, state) {
    super(props, children);
    this.state = state;
    this.setIds([
      'helloWorld'
    ]);
  }

  handleStateChange = (data) => {
    this.state = data.state;
    var cmd = '';
    switch (data.state.local.nextAction) {
      case "SAY_HELLO_WORLD":
        cmd = this.set({
          id: this.idSet.helloWorld,
          visibility: "visible",
        });
        console.log("SAY_HELLO_WORLD");
        break;
      case "SHOW_NEXT_SCREEN":
        this.props.showScreen("SHOW_SECOND_SCREEN", {});
        console.log("SHOW_SECOND_SCREEN");
        break;
      default:
        throw new Error("Next action not handled " + data.state.nextAction);
        break;
    }

    if (cmd) {
      return { runInUI: cmd }
    }
  }

  proceed = () => {
    this.props.proceed("I am awesome!");
  }

  render() {
    var width = window.__WIDTH + '';
    this.layout = (
      <LinearLayout 
				width="match_parent" 
				height="match_parent"
				background="#FFEEEE"
				root="true">
				<TextView 
					width="wrap_content"
					height="wrap_content"
					text="Hello World!"
					visibility="gone"
					id = {this.idSet.helloWorld}/>
				<Button
					width="wrap_content"
					height="wrap_content"
					text="Hello World!"
					onClick={this.props.sayHello}/>
				<Button
					width="wrap_content"
					height="wrap_content"
					text="NEXT"
					onClick={this.proceed}/>
			</LinearLayout>
    )
    return this.layout.render();
  }
}

module.exports = Connector(HelloWorldScreen);
