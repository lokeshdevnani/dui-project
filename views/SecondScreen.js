var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;

const CURRENT_SCREEN = "SECOND_SCREEN";

class SecondScreen extends View {

  constructor(props, children, state) {
    console.log("WELCOME TO SECOND SCREEN");
    super(props, children);
    this.state = state;
  }


  handleStateChange = (data) => {
    this.state = data.state;
    var cmd = '';
    switch (data.state.local.nextAction) {
      case "SHOW_NEXT_SCREEN":
        this.props.showScreen("HELLO_WORLD_SCREEN", {});
        console.log("HELLO_WORLD_SCREEN");
        break;
      default:
        throw new Error("Next action not handled " + data.state.nextAction);
        break;
    }
    return { runInUI: cmd };
  }

  handleBackPress = () => {
    this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  }

  render() {
    var width = window.__WIDTH + '';
    this.layout = (
      <LinearLayout 
				width="match_parent" 
				height="match_parent"
				background="#FFFFFF"
				root="true">
				<TextView 
					width="wrap_content"
					height="wrap_content"
					text="WELCOME TO SECOND SCREEN"/>
				<Button
					width="wrap_content"
					height="wrap_content"
					text="Take Me Back"
					onClick={this.handleBackPress}/>
			</LinearLayout>
    )
    return this.layout.render();
  }
}

module.exports = Connector(SecondScreen);
