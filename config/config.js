const config = {
  development: {
    backend: {
      domain: "http://upi-wrapper-beta.ap-south-1.elasticbeanstalk.com",
      endpoints: {
        bind: "/api/v1/devices?embed=registrationToken",
        tokenStatus: "/api/v1/tokens/",
        registration: "/api/v1/appUsers?embed=loginToken",
        getSmsDetails: "v1/healthcheck?version=1.3",
        login: "/api/v1/appUsers/:userId/loginToken",
        updateUser: "/api/v1/appUsers/:userId",
        getAllBanks: "/api/v1/banks",
        getAccounts: "/api/v1/accounts?",
        listKeys: "/api/v1/npci/keys",
        getNpciToken: "/api/v1/npci/token",
        requestOtp: "/api/v1/accounts/:accountId/otp",
        changeMpin: "/api/v1/accounts/:accountId",
        getAllAccounts: "/api/v1/accounts",
        pay: "/api/v1/transactions",
        transactionStatus: "/api/v1/transactions/:transactionId",
        transactionRequests: "/api/v1/transactionRequests",
        declineTransaction: "/api/v1/transactions/:transactionRequestId",
        listContacts: "/api/v1/contacts",
        updateContact: "/api/v1/contacts/:contactId",
        verifyVpa: "/api/v1/contacts/vpa/:beneVpa",
        listVpa:"/api/v1/vpas",
        linkVpaToAccount:"/api/v1/accounts/:accountId/vpa",
        changePasscode: "/api/v1/appUsers/",
        query : "/api/v1/transactions/:transactionId/queries",
        getAllQueries:"/api/v1/queries"
      },
      timeout: 60000
    }
  },
  beta: {
    backend: {
      domain: "https://pingupi.axisbank.co.in",
      endpoints: {
        bind: "/v1/bind",
        getSmsDetails: "v1/healthcheck?version=1.3",
        login: "/v1/authenticate"
      }
    }
  },
  production: {
    backend: {
      domain: "https://pingupi.axisbank.co.in",
      endpoints: {
        bind: "/bind"
      }
    }
  }
}


module.exports = config;
